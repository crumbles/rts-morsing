#include <string>
#include <iostream>
#include <map>
#include <vector>
#include <cstring>
#include <algorithm>
#include <stdexcept>
#include "morse_code.h"
#include <patterns/patterns.hpp>
#include <range/v3/all.hpp>

const std::vector<std::pair<char, std::vector<MorsePulse>>> MORSE_ENC = {
        {'A', {ON_1, OFF_1, ON_3}},
        {'B', {ON_3, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'C', {ON_3, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_1}},
        {'D', {ON_3, OFF_1, ON_1, OFF_1, ON_1}},
        {'E', {ON_1}},
        {'F', {ON_1, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_1}},
        {'G', {ON_3, OFF_1, ON_3, OFF_1, ON_1}},
        {'H', {ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'I', {ON_1, OFF_1, ON_1}},
        {'J', {ON_1, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3}},
        {'K', {ON_3, OFF_1, ON_1, OFF_1, ON_3}},
        {'L', {ON_1, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1}},
        {'M', {ON_3, OFF_1, ON_3}},
        {'N', {ON_3, OFF_1, ON_1}},
        {'O', {ON_3, OFF_1, ON_3, OFF_1, ON_3}},
        {'P', {ON_1, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_1}},
        {'Q', {ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_3}},
        {'R', {ON_1, OFF_1, ON_3, OFF_1, ON_1}},
        {'S', {ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'T', {ON_3}},
        {'U', {ON_1, OFF_1, ON_1, OFF_1, ON_3}},
        {'V', {ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_3}},
        {'W', {ON_1, OFF_1, ON_3, OFF_1, ON_3}},
        {'X', {ON_3, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_3}},
        {'Y', {ON_3, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_3}},
        {'Z', {ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1}},
        {'0', {ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3}},
        {'1', {ON_1, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3}},
        {'2', {ON_1, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3}},
        {'3', {ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_3}},
        {'4', {ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_3}},
        {'5', {ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'6', {ON_3, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'7', {ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_1}},
        {'8', {ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1}},
        {'9', {ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_1}},
        {'.', {ON_1, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_3}},
        {',', {ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_3}},
        {'?', {ON_1, OFF_1, ON_1, OFF_1, ON_3, OFF_1, ON_3, OFF_1, ON_1, OFF_1, ON_1}},
        {' ', {OFF_1}}
};

template<typename E>
std::vector<E> Replace(const std::vector<E> base, E source, std::vector<E> target) {
    std::vector<std::vector<E>> replaced_base =
            base
            | ranges::views::transform([source, target](E elem) {
                return (elem == source) ? std::vector<E>(target) : std::vector<E>{elem};
            })
            | ranges::to<std::vector<std::vector<E>>>();

    std::vector<E> result =
            replaced_base
            | ranges::views::join
            | ranges::to<std::vector<E>>();

    return result;
}

std::vector<MorsePulse> EncodePulse(std::vector<std::pair<char, std::vector<MorsePulse>>> encodings, char c) {
    auto element = ranges::find_if(encodings, [c](auto elem) { return toupper(c) == elem.first; });
    if (element == ranges::end(encodings)) {
        throw std::invalid_argument("Invalid morse symbol sequence received.");
    }
    return (*element).second;
}

char DecodePulse(std::vector<std::pair<char, std::vector<MorsePulse>>> encodings, std::vector<MorsePulse> pulses) {
    auto element = ranges::find_if(encodings, [pulses](auto elem) { return pulses == elem.second; });
    if (element == ranges::end(encodings)) {
        throw std::invalid_argument("Invalid morse symbol sequence received.");
    }
    return (*element).first;
}


std::vector<MorsePulse> EncodeMorse(const std::string text) {
    std::vector<char> chars(text.begin(), text.end());

    std::vector<std::vector<MorsePulse>> char_pulses =
            chars
            | ranges::views::transform([](char elem) { return EncodePulse(MORSE_ENC, elem); })
            | ranges::to<std::vector<std::vector<MorsePulse>>>();

    std::vector<std::vector<MorsePulse>> separated_pulses =
            char_pulses
            | ranges::views::intersperse(std::vector<MorsePulse>{OFF_3})
            | ranges::to<std::vector<std::vector<MorsePulse>>>();

    std::vector<MorsePulse> pulses =
            separated_pulses
            | ranges::views::join
            | ranges::to<std::vector<MorsePulse>>();

    return pulses;
}

std::string DecodeMorse(const std::vector<MorsePulse> pulses) {
    auto normalized_pulses = Replace(pulses, OFF_7, std::vector<MorsePulse>{OFF_3, OFF_1, OFF_3});
    std::vector<std::vector<MorsePulse>> char_pulses =
            normalized_pulses
            | ranges::views::split(OFF_3)
            | ranges::to<std::vector<std::vector<MorsePulse>>>();

    std::vector<char> chars =
            char_pulses
            | ranges::views::transform([](std::vector<MorsePulse> pulses) { return DecodePulse(MORSE_ENC, pulses); })
            | ranges::to<std::vector<char>>();

    std::string message(chars.begin(), chars.end());
    return message;
}


