#ifndef MORSE_RECEIVER_MORSE_RECEIVER_H
#define MORSE_RECEIVER_MORSE_RECEIVER_H

struct SensorState {
    int value;
    int duration;
    std::vector<MorsePulse> pulses;
    std::string message;
};

SensorState ComputeSensorStateUpdate(const SensorState previous_state, const int sensor_value);

#endif //MORSE_RECEIVER_MORSE_RECEIVER_H
