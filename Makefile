# the compiler: gcc for C program, define as g++ for C++
CC = g++ -std=c++20 -Isrc/lib/range-v3-master/include -Isrc/lib/mpark

# compiler flags:
#  -g     - this flag adds debugging information to the executable file
#  -Wall  - this flag is used to turn on most compiler warnings
CFLAGS  = -g -Wall -pthread
LFLAGS = -lpigpio -lrt

COMMON_OBJECTS = morse_code.o env.o timed_task.o
SND_OBJECTS = $(COMMON_OBJECTS) sender_main.o morse_sender.o
RCV_OBJECTS = $(COMMON_OBJECTS) receiver_main.o morse_receiver.o
ANA_OBJECTS = $(COMMON_OBJECTS) analyzer_main.o

GOALS = sender receiver analyzer

.PHONY: all clean
all: $(GOALS)

sender: $(SND_OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LFLAGS)

receiver: $(RCV_OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LFLAGS)

analyzer: $(ANA_OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^ $(LFLAGS)

%.o: $^
	$(CC) $(CFLAGS) -c -o $@ $<

# common
morse_code.o: src/common/morse_code.cpp src/common/morse_code.h
env.o: src/common/env.cpp src/common/env.h
timed_task.o: src/common/timed_task.cpp src/common/timed_task.h

# sender
sender_main.o: src/sender/sender_main.cpp
morse_sender.o: src/sender/morse_sender.cpp src/sender/morse_sender.h

# receiver
receiver_main.o: src/receiver/receiver_main.cpp
morse_receiver.o: src/receiver/morse_receiver.cpp

# analyzer
analyzer_main.o: src/analyzer/analyzer_main.cpp

clean:
	rm -f *.o $(GOALS)
