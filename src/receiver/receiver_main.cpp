#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include "../common/morse_code.h"
#include "../sender/morse_sender.h"
#include "../common/env.h"
#include <pigpio.h>
#include <patterns/patterns.hpp>
#include <functional>
#include "morse_receiver.h"
#include "../common/timed_task.h"

int ReceiveMessage(void *ptr) {
    auto sensor_value = gpioRead(GPIO_PIN_SEN);
    auto *io_ref = (struct SensorState *) ptr;
    struct SensorState previous_state = {
            io_ref->value,
            io_ref->duration,
            std::vector<MorsePulse>(io_ref->pulses),
            io_ref->message,
    };
    auto next_state = ComputeSensorStateUpdate(previous_state, sensor_value);
    io_ref->duration = next_state.duration;
    io_ref->value = next_state.value;
    io_ref->pulses = std::vector<MorsePulse>(next_state.pulses);

    using namespace mpark::patterns;
    return match(next_state.message)(
            pattern("") = [] {
                return 1;
            },
            pattern(_) = [next_state] {
                std::cout << "Transmission end received..." << std::endl;
                std::cout << "Message received: \"" + next_state.message + "\"" << std::endl;
                return 0;
            }
    );
}

int WaitRoutine(void *user_data) {
    auto sensor_value = gpioRead(GPIO_PIN_SEN);
    auto io_ref = SensorState{1, 0, std::vector<MorsePulse>{}};

    using namespace mpark::patterns;
    return match(sensor_value)(
            pattern(0) = [sensor_value, &io_ref] {
                std::cout << "Transmission start received..." << std::endl;
                std::this_thread::sleep_for(std::chrono::milliseconds(SYNC_DELAY));
                RunPeriodically(MORSE_UNIT_MS, &ReceiveMessage, &io_ref);
                return 0;
            },
            pattern(1) = [] { return 1; }
    );
}

int main() {
    if (gpioInitialise() < 0) {
        std::cerr << "GPIO interface unavailable." << std::endl;
        return 1;
    }
    gpioSetMode(GPIO_PIN_SEN, PI_INPUT);
    RunPeriodically(5, &WaitRoutine, nullptr);
    gpioTerminate();
    return 0;
}
