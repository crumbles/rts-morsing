#include <functional>
#include <chrono>
#include <thread>
#include <iostream>
#include <pigpio.h>
#include "timed_task.h"

std::vector<long> schedule_delay = {};

void _DebugSaveTimingDelay(std::chrono::time_point<std::chrono::system_clock> start, std::chrono::time_point<std::chrono::system_clock> actual_now, long off) {
    long start_ms = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::time_point_cast<std::chrono::microseconds>(start).time_since_epoch()).count();
    long now = std::chrono::duration_cast<std::chrono::microseconds>(actual_now.time_since_epoch()).count();
    long exp = start_ms + off * 1000;
    schedule_delay.push_back(now - exp);

//    std::cout << "expected = " + std::to_string(exp) + ", actual = " + std::to_string(now) + ", diff = " + std::to_string(now - exp) + "us" << std::endl;
}

void _DebugPrintAverageTimingInaccuracy() {
    long max = -1;
    long min = 5000;
    float avg = 0;
    for (auto delay : schedule_delay) {
        avg += delay;
        if (delay < min) {
            min = delay;
        }
        if (delay > max) {
            max = delay;
        }
    }
    avg = avg / schedule_delay.size();

    std::cout << "TIMING DELAYS:" << std::endl;
    std::cout << "min = " + std::to_string(min) + ", max = " + std::to_string(max) + ", avg = " + std::to_string(avg)
              << std::endl;
}


void RunPeriodically(const int interval_ms, const std::function<int (void*)> f, void *io_ref) {
    auto start_ts = std::chrono::system_clock::now();
    auto next_ts = std::chrono::system_clock::now();
    auto i = 0, f_res = 1;

    while(f_res > 0) {
        i++;
        next_ts = start_ts + std::chrono::milliseconds(i * interval_ms);
        std::this_thread::sleep_until(next_ts);
//        _DebugSaveTimingDelay(start_ts, std::chrono::system_clock::now(), i * interval_ms);
        f_res = f(io_ref);
    }
//    _DebugPrintAverageTimingInaccuracy();
}

