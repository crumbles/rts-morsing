#ifndef MORSE_RECEIVER_ENV_H
#define MORSE_RECEIVER_ENV_H

const int GPIO_PIN_LED = 18;
const int GPIO_PIN_SEN = 23;
const int MORSE_UNIT_MS = 200;
const int SYNC_UNIT_MS = 1000;
const int SYNC_DELAY = SYNC_UNIT_MS + (MORSE_UNIT_MS / 2) - 5;

#endif //MORSE_RECEIVER_ENV_H
