#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include <pigpio.h>
#include "../common/morse_code.h"
#include "../common/env.h"
#include "morse_sender.h"


int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Invalid arguments received" << std::endl;
        return 1;
    }
    if (gpioInitialise() < 0) {
        std::cerr << "GPIO interface unavailable" << std::endl;
        return 1;
    }
    gpioSetMode(GPIO_PIN_LED, PI_OUTPUT);
    std::string text = argv[1];
    std::cout << "Sending message..." << std::endl;
    SendText(GPIO_PIN_LED, MORSE_UNIT_MS, text);
    std::cout << "Finished sending message!" << std::endl;
    gpioTerminate();
    return 0;
}
