#ifndef MORSE_RECEIVER_MORSE_CODE_H
#define MORSE_RECEIVER_MORSE_CODE_H
#include <vector>
#include <string>

enum MorsePulse {
    ON_1, ON_3, OFF_1, OFF_3, OFF_7
};

std::vector<MorsePulse> EncodeMorse(std::string text);

std::string DecodeMorse(std::vector<MorsePulse> pulses);

#endif //MORSE_RECEIVER_MORSE_CODE_H
