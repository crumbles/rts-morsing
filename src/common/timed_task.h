#ifndef MORSE_RECEIVER_TIMED_TASK_H
#define MORSE_RECEIVER_TIMED_TASK_H
#include <functional>

void RunPeriodically(int interval_ms, std::function<int (void*)> f, void *io_ref);

#endif //MORSE_RECEIVER_TIMED_TASK_H
