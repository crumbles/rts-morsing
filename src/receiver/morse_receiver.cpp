#include <iostream>
#include <chrono>
#include <thread>
#include <vector>
#include "../common/morse_code.h"
#include "../sender/morse_sender.h"
#include "../common/env.h"
#include <pigpio.h>
#include <tuple>
#include <patterns/patterns.hpp>
#include <range/v3/all.hpp>
#include <ctgmath>
#include "morse_receiver.h"

MorsePulse ToPulse(const int sensor_value, const float duration) {
    std::cout << "Received "  + std::string((sensor_value == 0) ? "ON" : "OFF") + " with duration = " + std::to_string(duration) << std::endl;

    using namespace mpark::patterns;
    return match(sensor_value, duration)(
            pattern(0, 1) = [] { return ON_1; },
            pattern(0, 3) = [] { return ON_3; },
            pattern(1, 1) = [] { return OFF_1; },
            pattern(1, 3) = [] { return OFF_3; },
            pattern(1, 7) = [] { return OFF_7; }
    );
}

SensorState ComputeSensorStateUpdate(const SensorState previous_state, const int sensor_value) {
    using namespace mpark::patterns;
    auto value_change = previous_state.value != sensor_value;
    auto begin_of_transmission = previous_state.value == 1 && previous_state.pulses.empty();
    auto end_of_transmission = previous_state.duration > 7 && !previous_state.pulses.empty();

    return match(value_change, begin_of_transmission, end_of_transmission)(
            pattern(true, true, _) = [sensor_value, previous_state]() -> SensorState {
                return {sensor_value, 1, previous_state.pulses, ""};
            },
            pattern(true, false, _) = [sensor_value, previous_state]() -> SensorState {
                auto pulse = ToPulse(previous_state.value, previous_state.duration);
                auto pulses = std::vector<MorsePulse>(previous_state.pulses);
                pulses.push_back(pulse);
                return {sensor_value, 1, pulses, ""};
            },
            pattern(false, _, false) = [previous_state]() -> SensorState {
                return {previous_state.value, previous_state.duration + 1, previous_state.pulses, ""};
            },
            pattern(false, _, true) = [previous_state]() -> SensorState {
                return {1, 0, std::vector<MorsePulse>{}, DecodeMorse(previous_state.pulses)};
            }
    );
}