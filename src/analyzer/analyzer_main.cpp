#include <iostream>
#include <vector>
#include "../common/morse_code.h"
#include "../common/env.h"
#include <pigpio.h>
#include <cstdlib>
#include <iostream>
#include <patterns/patterns.hpp>
#include <range/v3/all.hpp>

uint32_t last_tick = 0;
std::vector<uint32_t> intervals = {};

void TestTest() {
    auto pulses = EncodeMorse("HELLO WORLD.");
    auto message = DecodeMorse(pulses);
    std::cout << "Your message was: " + message << std::endl;

}

void gpioTimerFunc() {
    uint32_t tick = gpioTick();
    if(last_tick != 0) {
        uint32_t diff = tick - last_tick;
        std::cout << "call time = " << std::to_string(diff) << std::endl;
        intervals.push_back(diff);
    }
    last_tick = tick;

}

void TimerTest() {
    gpioSetTimerFunc(0, MORSE_UNIT_MS, &gpioTimerFunc);
}

void sensorTest() {
	gpioSetMode(GPIO_PIN_SEN, PI_INPUT);
	std::cout << "Sensor Value: " << gpioRead(GPIO_PIN_SEN) << std::endl;
}

void LEDTimingTest() {
    uint32_t t1 = gpioTick();
    gpioWrite(18, 1);
    uint32_t actual_delay = gpioDelay(100000);
    gpioWrite(18, 0);
    uint32_t t2 = gpioTick();

    int diff_tick = t2 - t1;
    std::cout << "Total time:" + std::to_string(diff_tick) << "us" << std::endl;
    std::cout << "Thread sleep:" + std::to_string(actual_delay) << "us" << std::endl;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cerr << "Invalid number of arguments received. Specify one argument only." << std::endl;
        return 1;
    }
    if (gpioInitialise() < 0) {
        std::cerr << "GPIO interface could not be initialized." << std::endl;
        return 1;
    }

    std::cout << "GPIO interface initialized successfully..." << std::endl;
    int choice = atoi(argv[1]);

    switch(choice) {
        case 1:
            sensorTest();
            break;
        case 2:
            LEDTimingTest();
            break;
        case 3:
            TimerTest();
	    break;
        case 4:
            TestTest();
            break;
    }

    std::cout << "Press any key to exit..." << std::endl;

    int n;
    std::cin >> n;

    gpioTerminate();
    return 0;
}
