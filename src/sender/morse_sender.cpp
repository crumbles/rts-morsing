#include <string>
#include <pigpio.h>
#include <thread>
#include <iostream>
#include "morse_sender.h"
#include "../common/morse_code.h"
#include "../common/env.h"
#include "../common/timed_task.h"
#include <patterns/patterns.hpp>
#include <range/v3/all.hpp>

std::vector<int> ConvertPulse(MorsePulse pulse) {
    using namespace mpark::patterns;
    return match(pulse)(
            pattern(ON_1) = [] {
                return std::vector<int>{1};
            },
            pattern(ON_3) = [] {
                return std::vector<int>{1, 1, 1};
            },
            pattern(OFF_1) = [] {
                return std::vector<int>{0};
            },
            pattern(OFF_3) = [] {
                return std::vector<int>{0, 0, 0};
            },
            pattern(OFF_7) = [] {
                return std::vector<int>{0, 0, 0, 0, 0, 0, 0};
            }
    );
}

std::vector<int> ConvertPulses(const std::vector<MorsePulse> pulses) {
    std::vector<std::vector<int>> int_pulses =
            pulses
            | ranges::views::transform([](MorsePulse pulse) { return ConvertPulse(pulse); })
            | ranges::to<std::vector<std::vector<int>>>();
    std::vector<int> result =
            int_pulses
            | ranges::views::join
            | ranges::to<std::vector<int>>();

    return result;
}

void BeginTransmission(const int gpio, const int sync_interval) {
    gpioWrite(gpio, 1);
    std::this_thread::sleep_for(std::chrono::milliseconds(sync_interval));
    gpioWrite(gpio, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(sync_interval));
}

int SendPulse(void *io_ref) {
    auto *sender_ref = (struct SenderRef *) io_ref;
    auto end_of_stream =
            sender_ref->position == sender_ref->pulses.size();

    using namespace mpark::patterns;
    return match(end_of_stream)(
            pattern(true) = [] { return 0; },
            pattern(false) = [sender_ref] {
                gpioWrite(
                        GPIO_PIN_LED,
                        sender_ref->pulses.at(sender_ref->position)
                );
                sender_ref->position++;
                return 1;
            }
    );
}

void SendText(const int gpio, const int unit_length, const std::string text) {
    std::vector<MorsePulse> pulses = EncodeMorse(text);
    std::vector<int> int_pulses = ConvertPulses(pulses);
    struct SenderRef io_ref = {0, int_pulses};
    BeginTransmission(gpio, SYNC_UNIT_MS);
    RunPeriodically(MORSE_UNIT_MS, &SendPulse, &io_ref);
    gpioWrite(GPIO_PIN_LED, 0);
}
