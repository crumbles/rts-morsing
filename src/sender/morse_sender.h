#ifndef MORSE_RECEIVER_MORSE_SENDER_H
#define MORSE_RECEIVER_MORSE_SENDER_H
#include "../common/morse_code.h"

struct SenderRef {
    int position;
    std::vector<int> pulses;
};

void SendText(int gpio, int unit_length, std::string text);

#endif //MORSE_RECEIVER_MORSE_SENDER_H